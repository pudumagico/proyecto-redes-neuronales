import random
import time
import redNeural
import numpy as np

datafile = "TrainingExperimento2.data"

with open(datafile) as f:
    lines = f.readlines()
random.shuffle(lines)
with open("outfile.txt", "w+") as f:
    f.writelines(lines)

n_inputs = 1
n_outputs = 1
learnrate = 1

neuralNetwork = redNeural.redNeural(n_inputs,learnrate)
neuralNetwork.addLayer(2)
neuralNetwork.addLayer(2)
neuralNetwork.addLayer(n_outputs)

epochs = 10
excersises = 19875
testing = 6625

neuralNetwork.initialize(epochs)

start = time.time()
for i in range(epochs):
    print "epoch"
    print i
    random.shuffle(lines)
    with open("outfile.txt", "w+") as f:
        f.writelines(lines)
    with open("outfile.txt") as f:
        for j in range(excersises):
            data = f.readline()
            splitdata = data.split(",")
            inputs = splitdata[0]
            realinputs = float(inputs)
            # realinputs = []
            # for x in inputs:
            #     realinputs.append(x)
            targets = splitdata[1]
            realtargets = float(targets)
            # realtargets = []
            # for x in targets:
            #     realtargets.append(float(x))

            if i == epochs-1:
                neuralNetwork.finalOutputs.append(neuralNetwork.train(1, realinputs, realtargets, i))
            else:
                neuralNetwork.train(1, realinputs, realtargets, i)
end = time.time()

print "Tiempo total de entrenamiento (s)"
print (end - start)

aciertos = 0
with open("TestingExperimento2.data") as f:
    for j in range(testing):
        data = f.readline()
        splitdata = data.split(",")
        inputs = splitdata[0]
        realinputs = float(inputs)

        targets = splitdata[1]
        realtargets = float(targets)
        neuralNetwork.test(1,realinputs,realtargets)


floattesting = float(testing)

accuracytest = neuralNetwork.VP + neuralNetwork.VN
print "Accuracy en datos de testing (%)"
print accuracytest/floattesting
neuralNetwork.plotPerformance(epochs)
