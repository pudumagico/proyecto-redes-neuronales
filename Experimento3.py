import random
import time
import redNeural
import numpy as np

datafile = "TrainingExperimento3.data"

with open(datafile) as f:
    lines = f.readlines()
random.shuffle(lines)
with open("outfile.txt", "w+") as f:
    f.writelines(lines)

n_inputs = 2
n_outputs = 1
learnrate = 1

neuralNetwork = redNeural.redNeural(n_inputs,learnrate)
neuralNetwork.addLayer(2)
neuralNetwork.addLayer(2)
neuralNetwork.addLayer(n_outputs)

epochs = 10
excersises = 14250
testing = 4750

neuralNetwork.initialize(epochs)

start = time.time()
for i in range(epochs):
    print "epoch"
    print i
    random.shuffle(lines)
    with open("outfile.txt", "w+") as f:
        f.writelines(lines)
    with open("outfile.txt") as f:
        for j in range(excersises):
            data = f.readline()
            splitdata = data.split(",")
            inputs = splitdata[0:2]
            # realinputs = float(inputs)
            realinputs = []
            for x in inputs:
                realinputs.append(float(x))
            targets = splitdata[2]
            realtargets = float(targets)
            # realtargets = []
            # for x in targets:
            #     realtargets.append(float(x))

            if i == epochs-1:
                neuralNetwork.finalOutputs.append(neuralNetwork.train(1, realinputs, realtargets, i))
            else:
                neuralNetwork.train(1, realinputs, realtargets, i)
end = time.time()

print "Tiempo total de entrenamiento (s)"
print (end - start)

aciertos = 0
with open("TestingExperimento3.data") as f:
    for j in range(testing):
        data = f.readline()
        splitdata = data.split(",")
        inputs = splitdata[0:2]
        realinputs = []
        # realinputs = float(inputs)
        for x in inputs:
            realinputs.append(float(x))
        targets = splitdata[2]

        realtargets = float(targets)

        neuralNetwork.test(1,realinputs,realtargets)


floattesting = float(testing)

accuracytest = neuralNetwork.VP + neuralNetwork.VN
print "Accuracy en datos de testing (%)"
print accuracytest/floattesting
neuralNetwork.plotPerformance(epochs)
