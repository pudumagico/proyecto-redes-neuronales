import numpy as np
import matplotlib.pyplot as plt
import unittest
import neurona
import random

class capa:
    def __init__(self, numberOfNeurons, numberOfInputs, learnRate):
        self.learnRate = learnRate
        self.neurons = []
        for i in range(numberOfNeurons):
            weights = np.random.uniform(0,1,numberOfInputs)
            bias = random.uniform(0,1)
            newNeuron = neurona.neurona(weights, bias, self.learnRate)
            self.neurons.append(newNeuron)
        self.numberOfNeurons = numberOfNeurons
        self.previousLayer = False
        self.nextLayer = False
        self.isFirstLayer = False
        self.isLastLayer = False
        self.lastOutput = False

    def feed(self, inputs):
        output = []
        for neuron in self.neurons:
            z = neuron.output(inputs)
            output.append(z)
        if self.isLastLayer:
            self.lastOutput = output
            return output
        else:
            self.lastOutput = output
            return self.nextLayer.feed(output)

    def backPropagation(self, target):
        i = 0
        if self.isLastLayer:
            for neuron in self.neurons:
                td = neuron.transferDerivative()
                error = np.subtract(target, neuron.lastOutput)
                delta = np.multiply(td, error)
                neuron.captureDelta(delta)
                i += 1
            if self.isFirstLayer:
                pass
            else:
                self.previousLayer.backPropagation(None)
        else:
            for neuron in self.neurons:
                td = neuron.transferDerivative()
                error = 0
                for nextNeuron in self.nextLayer.neurons:
                    error += nextNeuron.weightXdelta(i)
                i += 1
                delta = np.multiply(td, error)
                neuron.captureDelta(delta)
            if self.isFirstLayer:
                pass
            else:
                self.previousLayer.backPropagation(None)

    def learn(self):
        for neuron in self.neurons:
            neuron.learn()
