Para correr los experimentos basta ejecutar en una consola ubicada en este
directorio el siguiente comando:

$python ExperimentoX.py

Donde la X puede ser 1,2,3 o 4 segun el experimento que desee ejecutar. A
continuacion hay una breve descripcion de cada experimento:

1. Clases balanceadas, solo una caracteristica.
2. Clases desbalanceadas, solo una caracteristica.
3, Clases balanceadas, dos caracteristicas, la anterior y otra inventada.
4, Clases desbalanceadas, dos caracteristicas, la anterior y otra inventada.

La caracteristica inventada corresponde a una transformacion de la otra
caracteristica. Mas detalles en el informe.

Los archivos ejecutables son scripts que entrenan y prueban la red. Estos
contienen parametros que pueden ser modificados, como el learn rate y
el numero de epocas. Para evitar complicaciones, sugerimos solo jugar con esos
parametros.

La carpeta "base de datos original" contiene los datos originales, que luego
seran transformados para entrenar la red.

La carpeta "imagenes" contiene algunos graficos de los resultados.

La carpeta "utiles" contiene scripts que permiten transformar datos para poder
entrenar la red.
