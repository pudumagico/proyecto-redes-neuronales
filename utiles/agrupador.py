import csv

with open('TestingExperimento1.data', 'wb') as test:
    writertest = csv.writer(test)
    with open('TrainingExperimento1.data', 'wb') as train:
        writertrain = csv.writer(train)
        with open('EB_shuffled.csv', 'rb') as eb:
            readereb = csv.reader(eb, delimiter=',', quotechar='|')
            with open('RRL_shuffled.csv', 'rb') as rrl:
                readerrrl = csv.reader(rrl, delimiter=',', quotechar='|')

                totaltrain = 7125
                totaltest = 2375

                for i in range(totaltrain):
                    writertrain.writerow(readereb.next())
                for i in range(totaltrain):
                    writertrain.writerow(readerrrl.next())

                for i in range(totaltest):
                    writertest.writerow(readereb.next())
                for i in range(totaltest):
                    writertest.writerow(readerrrl.next())
