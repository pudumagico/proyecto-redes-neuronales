import random

fid = open("EB_transformado.csv", "r")
header = fid.readline()
li = fid.readlines()
fid.close()

random.shuffle(li)

fid = open("EB_shuffled.csv", "w")
# fid.writelines(header)
fid.writelines(li)
fid.close()

fid = open("RRL_transformado.csv", "r")
header = fid.readline()
li = fid.readlines()
fid.close()

random.shuffle(li)

fid = open("RRL_shuffled.csv", "w")
# fid.writelines(header)
fid.writelines(li)
fid.close()
